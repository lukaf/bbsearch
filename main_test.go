package main

import (
	"reflect"
	"testing"
	"time"
)

func TestDumpLoad(t *testing.T) {
	source := &Config{
		Projects:     []string{"P1", "P2", "P3"},
		Token:        "secret123",
		LastModified: time.Now().UTC(),
	}

	destination := &Config{}

	data, err := source.dump()
	if err != nil {
		t.Fatal(err)
	}

	if err := destination.load(data); err != nil {
		t.Fatal(err)
	}

	if !reflect.DeepEqual(source, destination) {
		t.Fatal("Source and destination configurations are not the same")
	}
}

func TestParseProjectsString(t *testing.T) {
	input := "P1 P2    P3,P4, P5"
	want := "P1,P2,P3,P4,P5"

	got := parseProjectsString(input)
	if want != got {
		t.Fatalf("Want '%s', got '%s'\n", want, got)
	}
}

func TestAppendNewProjects(t *testing.T) {
	newProjects := []string{"P1", "P2"}
	existingProjects := []string{"P1", "P3"}

	want := []string{"P1", "P2", "P3"}
	got := appendNewProjects(existingProjects, newProjects)

	if !reflect.DeepEqual(want, got) {
		t.Fatalf("Want '%s', got '%s'", want, got)
	}
}
