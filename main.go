package main

import (
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	"io/ioutil"
	"os"
	"path"
	"regexp"
	"sort"
	"strings"
	"time"
)

func main() {
	configSubCmd := flag.NewFlagSet("config", flag.ExitOnError)
	projects := configSubCmd.String("p", "", "Projects to add to search list")
	token := configSubCmd.String("t", "", "Personal token for authentication against Bitbucket")

	findSubCmd := flag.NewFlagSet("find", flag.ExitOnError)
	repository := findSubCmd.String("r", "", "Repository to find")

	if len(os.Args) < 3 {
		fmt.Println(usage())
		os.Exit(-1)
	}

	switch os.Args[1] {
	case "config":
		configSubCmd.Parse(os.Args[2:])
		config(*projects, *token)

	case "find":
		findSubCmd.Parse(os.Args[2:])
		find(*repository)

	default:
		fmt.Printf("Unknown subcommand: %s\n", os.Args[1])
		fmt.Println(usage())
		os.Exit(-1)
	}
}

func configPath() string {
	dirname, err := os.UserHomeDir()
	if err != nil {
		fmt.Printf("Unable to find user's home directory: %s", err)
		os.Exit(-1)
	}

	return path.Join(dirname, ".bbsearch.config")
}

func usage() string {
	return fmt.Sprintf("Usage: %s [config|find] [-t token -p P1 P2]|[-r repository]\n", os.Args[0])
}

func config(projects string, token string) {
	if projects != "" {
		projects = parseProjectsString(projects)
	}

	config := &Config{}
	err := config.read(configPath())
	if err != nil && !errors.Is(err, os.ErrNotExist) {
		fmt.Printf("ERROR: Unable to read configuration: %s", err)
		os.Exit(1)
	}

	config.Projects = appendNewProjects(config.Projects, strings.Split(projects, ","))
	if token != "" {
		config.Token = token
	}

	config.LastModified = time.Now().UTC()

	if err := config.write(configPath()); err != nil {
		fmt.Printf("WARN: Unable to write configuration: %s", err)
	}

	if config.Token == "" {
		fmt.Printf("WARN: configuration doesn't inlude a token")
	}

	if len(config.Projects) == 0 {
		fmt.Printf("WARN: configuration doesn't include any projects")
	}
}

func appendNewProjects(dst []string, src []string) []string {
	projects := make([]string, len(dst))
	copy(projects, dst)

LOOP:
	for _, newProject := range src {
		for _, existingProject := range projects {
			if newProject == existingProject {
				continue LOOP
			}
		}
		projects = append(projects, newProject)
	}

	sort.Strings(projects)
	return projects
}

func parseProjectsString(input string) string {
	re := regexp.MustCompile(`[\t ,]+`)
	return re.ReplaceAllString(input, ",")
}

func find(repository string) {
	config := &Config{}

	err := config.read(configPath())
	if errors.Is(err, os.ErrNotExist) {
		fmt.Println("ERROR: Configuration doesn't exist, use 'config' subcommand to create it")
		os.Exit(1)
	}

	if err != nil {
		fmt.Printf("ERROR: Unable to read configuration: %s", err)
		os.Exit(1)
	}

	fmt.Printf("Searching for '%s' using '%#v'\n", repository, config)
}

type Config struct {
	Projects     []string
	Token        string
	LastModified time.Time
}

func (c *Config) load(data []byte) error {
	return json.Unmarshal(data, &c)
}

func (c *Config) read(filename string) error {
	data, err := ioutil.ReadFile(filename)
	if err != nil {
		return err
	}

	return c.load(data)
}

func (c *Config) dump() ([]byte, error) {
	return json.Marshal(c)
}

func (c *Config) write(filename string) error {
	data, err := c.dump()
	if err != nil {
		return err
	}

	return ioutil.WriteFile(filename, data, 0640)
}
